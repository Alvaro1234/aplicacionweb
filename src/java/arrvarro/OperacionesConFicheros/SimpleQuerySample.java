
package arrvarro.OperacionesConFicheros;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Álvaro Paradas Borrego
 */
@WebServlet(name = "SimpleQuerySample", urlPatterns = {"/SimpleQuerySample"})
public class SimpleQuerySample extends HttpServlet {
    
    // declaro los valores literales que va a tomar el switch
    public static final byte GET = 0; // select
    public static final byte POST = 1; // insert
    public static final byte PUT = 2; // updating
    public static final byte DELETE = 3; // remove
    
    // declaramos la expresion del switch
    private byte expresionSwitch;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    // declaramos un objeto persona
    Persona persona = new Persona();
    // conectamos con la base de datos persostence.xml
    EntityManager entityManager = Persistence.createEntityManagerFactory("WebApplication1PU").createEntityManager();
    // declaro una lista de personas
    List<Persona> listaPersonas;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JAXBException {
//        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try  {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet SimpleQuerySample</title>");            
//            out.println("</head>");
//            out.println("<body>");//            
//            // cargamos una consulta en el objeto query
            Query query = entityManager.createNamedQuery("Persona.findAll");
//            // guardamos en un array de personas, las personas obtetidas de el objeto query
            listaPersonas = query.getResultList();            
//            // bucle para mostrar datos de las personas
//            for (Persona persona : personas) {
//                System.out.println(persona.getNombre() + ", " + persona.getApellidos());  
//                out.println(persona.getNombre() + ", " + persona.getApellidos() + "<br>");  
//            }
            // declaro un objero personas, la listaPersonas de la clase personas esta vacia
            Personas personas = new Personas();
            // paso la lista de personas a un objeto de personas para que no de error el marshal
            personas.setLista(listaPersonas);
            
            
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller(); 
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            //jaxbMarshaller.marshal(persona, out);
            // iniciamos la transaccion con la base de datos
            entityManager.getTransaction().begin(); 
            switch(expresionSwitch) {
                case GET:
                    // Convertimos a XML el resultado obtenido de la salida out
                    // personas = lo que vamos a transformar a XML
                    // out = por donde vamos a sacar el XML, la salida del servidor 
                    jaxbMarshaller.marshal(personas, out);
                    break;
                case POST:
                    // Obtenemos la lista de personas que se quieren insertar
                    Personas newPersonas = (Personas) jaxbUnmarshaller.unmarshal(request.getInputStream());
                    // añadimos esa persona a la base de datos                    
                    entityManager.persist(persona);                       
                    // Recorremos la lista para obtener los objetos contenidos en ella
                    for(Persona persona :  newPersonas.getListaPersonas()) {
                        // Añadir cada objeto a la lista general
                        personas.getListaPersonas().add(persona);
                    }
                    break;
                case PUT:
                    // actualizamos a un persona
                    entityManager.merge(persona);
                    break;
                case DELETE:
                    
                    break;
            }
            // terminamos la transaccion con la base de datos
            entityManager.getTransaction().commit();
            
//            out.println("<h1>Servlet SimpleQuerySample at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
        } catch (JAXBException ex) {
            Logger.getLogger(SimpleQuerySample.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            expresionSwitch = GET;
        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(SimpleQuerySample.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    public void getAtenderPeticionPut(Persona persona){
        entityManager.getTransaction().begin(); 
         
        entityManager.getTransaction().commit();   

    }
    
    public void getAtenderPeticionDelete(Persona persona){
        entityManager.getTransaction().begin(); 
        entityManager.remove(persona); 
        entityManager.getTransaction().commit();   
        // borramos el objeto persona de la lista de individuos
        listaPersonas.remove(persona);
    }
    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(SimpleQuerySample.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
